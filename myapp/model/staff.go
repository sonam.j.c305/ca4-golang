package model

import (
	"goapp/myapp/DataStore/postgres"
)

type Staff struct {
	Staffid        int64  `json:"sid"`
	FirstName      string `json:"fname"`
	Lastname       string `json:"lname"`
	Specialization string `json:"specialization"`
}

const queryInsertStaff = "INSERT INTO staff(staff_id, firstname,lastname,specialization) VALUES($1,$2,$3,$4);"

const queryGetStaff = "SELECT * from staff where staff_id = $1"

const queryDeleteStaff = "DELETE FROM staff WHERE staff_id=$1;"

const queryGetTotalcount = "SELECT COUNT(*) FROM staff;"

func Gettotalst() (int, error) {
	var count int
	err := postgres.Db.QueryRow(queryGetTotalcount).Scan(&count)
	if err != nil {
		return 0, err
	}
	return count, nil
}

func (s *Staff) Create() error {
	_, err := postgres.Db.Exec(queryInsertStaff, s.Staffid, s.FirstName, s.Lastname, s.Specialization)
	return err
}

func (s *Staff) GetSta() error {
	return postgres.Db.QueryRow(queryGetStaff, s.Staffid).Scan(&s.Staffid, &s.FirstName, &s.Lastname, &s.Specialization)
}

func GetAllSta() ([]Staff, error) {
	rows, getErr := postgres.Db.Query("SELECT * from staff;")
	if getErr != nil {
		return nil, getErr
	}

	staffs := []Staff{}

	for rows.Next() {
		var s Staff
		dbErr := rows.Scan(&s.Staffid, &s.FirstName, &s.Lastname, &s.Specialization)
		if dbErr != nil {
			return nil, dbErr
		}
		staffs = append(staffs, s)
	}
	rows.Close()

	return staffs, nil
}
func (s *Staff) Delete() error {
	if _, err := postgres.Db.Exec(queryDeleteStaff, s.Staffid); err != nil {
		return err
	}
	return nil
}
