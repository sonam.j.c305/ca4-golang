package model

import (
	"goapp/myapp/DataStore/postgres"
)

type Player struct {
	Playerid  int64  `json:"pid"`
	FirstName string `json:"fname"`
	Lastname  string `json:"lname"`
	Kitnumber int64  `json:"kitnumber"`
}

const queryInsertUser = "INSERT INTO player(playerid, firstname,lastname,kit_number) VALUES($1,$2,$3,$4);"

const queryGetUser = "SELECT * from player where playerid = $1"

const queryDeleteUser = "DELETE FROM player WHERE playerid=$1;"

const queryGetTotalCount = "SELECT COUNT(*) FROM player;"

func (s *Player) Create() error {
	_, err := postgres.Db.Exec(queryInsertUser, s.Playerid, s.FirstName, s.Lastname, s.Kitnumber)
	return err
}

func (s *Player) Getplee() error {
	return postgres.Db.QueryRow(queryGetUser, s.Playerid).Scan(&s.Playerid, &s.FirstName, &s.Lastname, &s.Kitnumber)
}

// func (s *Player) Getotal() error {
// 	return postgres.Db.QueryRow(queryGetUser, s.Playerid).Scan(&s.Playerid, &s.FirstName, &s.Lastname, &s.Kitnumber)
// }

func Gettotal() (int, error) {
	var count int
	err := postgres.Db.QueryRow(queryGetTotalCount).Scan(&count)
	if err != nil {
		return 0, err
	}
	return count, nil
}

func (s *Player) Delete() error {
	if _, err := postgres.Db.Exec(queryDeleteUser, s.Playerid); err != nil {
		return err
	}
	return nil
}

func GetAllPlay() ([]Player, error) {
	rows, getErr := postgres.Db.Query("SELECT * from player;")
	if getErr != nil {
		return nil, getErr
	}

	players := []Player{}

	for rows.Next() {
		var s Player
		dbErr := rows.Scan(&s.Playerid, &s.FirstName, &s.Lastname, &s.Kitnumber)
		if dbErr != nil {
			return nil, dbErr
		}
		players = append(players, s)
	}
	rows.Close()

	return players, nil
}
