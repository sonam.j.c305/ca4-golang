function signUp(){
    // data to be sent to the POST request
    var _data = {
    fname : document.getElementById("fname").value,
    lname : document.getElementById("lname").value,
    cid : document.getElementById("CID").value,
    password : document.getElementById("pw1").value,
    pw : document.getElementById("pw2").value
    }

    if (_data.password !== _data.pw) {
      var popup = document.createElement("div");
      popup.innerHTML = "PASSWORD doesn't match!";
      popup.style.backgroundColor = "blue";
      popup.style.color = "white";
      popup.style.padding = "10px";
      popup.style.position = "fixed";
      popup.style.top = "50%";
      popup.style.left = "50%";
      popup.style.transform = "translate(-50%, -50%)";
      popup.style.zIndex = "9999";
      document.body.appendChild(popup);
      return;
    }
    fetch('/signup', {
    method: "POST",
    body: JSON.stringify(_data),
    headers: {"Content-type": "application/json; charset=UTF-8"}
    })
    .then(response => {
    if (response.status == 201) {
    // console.log("logged in")
    console.log(_data);
    window.open("login.html", "_self")
    }
    })
}    



