
window.onload = function (){
    fetch('/staffs', {
        method: "GET"
    })
        .then(response => response.text())
        .then(_data => showStaffs(_data))
}


function showStaff(_data) {
    var search_data = {
        sid : parseInt(document.getElementById("staff-id").value),
    }
    const staff = JSON.parse(search_data)
    newRow(staff)
}

function newRow(staff) {
    var table = document.getElementById("myTable");
    var row = table.insertRow(table.length);
    var td = []
    for( i=0; i<table.rows[0].cells.length; i++){
        td[i] = row.insertCell(i)
    }
    
    td[0].innerHTML = staff.sid;
    td[0].style.border = "1px solid black"; // Add border style to the first cell
    td[1].innerHTML = staff.fname;
    td[1].style.border = "1px solid black"; // Add border style to the second cell
    td[2].innerHTML = staff.lname;
    td[2].style.border = "1px solid black"; // Add border style to the third cell
    td[3].innerHTML = staff.specialization;
    td[3].style.border = "1px solid black"; // Add border style to the fourth cell
    td[4].innerHTML = '<input type="button" onclick="deleteStaff(this)" value="del" id="button-1">';
    td[4].style.border = "1px solid black"; // Add border style to the fifth cell

    
    td[0].innerHTML = staff.sid;
    td[0].style.border = "1px solid black";
    td[0].style.textAlign = "center";

    td[1].innerHTML = staff.fname;
    td[1].style.border = "1px solid black";
    td[1].style.textAlign = "center";

    td[2].innerHTML = staff.lname;
    td[2].style.border = "1px solid black";
    td[2].style.textAlign = "center";

    td[3].innerHTML = staff.specialization;
    td[3].style.border = "1px solid black";
    td[3].style.textAlign = "center";

    td[4].innerHTML = '<input type="button" onclick="deleteStaff(this)" value="del" id="button-1">';
    td[4].style.border = "1px solid black";
    td[4].style.textAlign = "center";

}

function showStaffs(_data){
    const staffs = JSON.parse(_data)
    staffs.forEach(staf => {
        newRow(staf)
    });
}


function deleteStaff(r){
    if(confirm('Are you sure you want to DELETE this ?')){
        selectedRow = r.parentElement.parentElement;
        Sid = selectedRow.cells[0].innerHTML;

        fetch('/staff/'+Sid, {
            method : "DELETE",
            headers : {"Content-type": "application/json; charset=UTF-8"}
        });
        var rowIndex = selectedRow.rowIndex;
        if(rowIndex>0){
            document.getElementById("myTable").deleteRow(rowIndex);
        }
        selectedRow = null;
    }
}
