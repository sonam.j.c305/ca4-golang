
function addPlayer(){
  var data = {
      pid : parseInt(document.getElementById("player-id").value),
      fname : document.getElementById("firstname").value,
      lname : document.getElementById("lastname").value,
      kitnumber : parseInt(document.getElementById("kit-number").value)
  }


  fetch('/player', {
      method: "POST",
      body: JSON.stringify(data),
      headers: {"Content-type": " application; charset=UTF-8"}  // not mandotary this header
  }).then(response1 => {
        if(response1.status === 201){
            window.open("addplayer.html", "_self")
            alert("SUCCESS!")
            return
        }
        else{
            throw "Not SUCCESSFUL"
        }
      
  }).catch (e => {
    //   alert(e)
    console.log(e);                // alert the error
  })
}