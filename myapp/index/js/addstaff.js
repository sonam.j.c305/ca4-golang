
function addStaff(){
    var _data = {
        sid : parseInt(document.getElementById("staff-id").value),
        fname : document.getElementById("firstname").value,
        lname : document.getElementById("lastname").value,
        specialization : document.getElementById("specialization").value
    }
  
  
    fetch('/staff', {
        method: "POST",
        body: JSON.stringify(_data),
        headers: {"Content-type": " application; charset=UTF-8"}  // not mandotary this header
    }).then(response1 => {
          if(response1.status === 201){
              window.open("addstaff.html", "_self")
              alert("SUCCESS!")
              return
          }
          else{
              throw "Not SUCCESSFUL"
          }
        
    }).catch (e => {
      //   alert(e)
      console.log(e);                // alert the error
    })
  }