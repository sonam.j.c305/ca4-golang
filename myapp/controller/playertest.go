package controller

import (
	"bytes"
	"io"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestAddPlayer(t *testing.T) {
	url := "http://localhost:8080/player"

	var jsonStr = []byte(`{"pid":"1234", "fname":"Kinga", "lname":"Tshering", "kitnumber":7}`)

	req, _ := http.NewRequest("POST", url, bytes.NewBuffer(jsonStr))

	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)

	if err != nil {
		panic(err)
	}

	defer resp.Body.Close()

	body, _ := io.ReadAll(resp.Body)

	assert.Equal(t, http.StatusCreated, resp.StatusCode)
	expResp := `{"status": "player added"}`

	assert.JSONEq(t, expResp, string(body))
}

func TestGetPlayer(t *testing.T) {
	c := &http.Client{}
	r, _ := c.Get("http://localhost:8080/player/1234")
	body, _ := io.ReadAll(r.Body)
	assert.Equal(t, http.StatusOK, r.StatusCode)
	expResp := `{"pid":"1006", "fname":"Kinga", "lname":"Tshering", "kitnumber":7}`
	assert.JSONEq(t, expResp, string(body))
}

func TestDeletePlayer(t *testing.T) {
	url := "http://localhost:8080/player/1234"

	req, _ := http.NewRequest("DELETE", url, nil)

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()

	body, _ := io.ReadAll(resp.Body)
	assert.Equal(t, http.StatusOK, resp.StatusCode)
	expResp := `{"status": "deleted"}`
	assert.JSONEq(t, expResp, string(body))

}

func TestPlayerNotFound(t *testing.T) {
	assert := assert.New(t)
	c := &http.Client{}
	r, _ := c.Get("http://localhost:8080/player/1006")
	body, _ := io.ReadAll(r.Body)
	assert.Equal(http.StatusNotFound, r.StatusCode)
	expResp := `{"error":"Student not found"}`
	assert.JSONEq(expResp, string(body))
}
