package controller

import (
	"database/sql"
	"encoding/json"
	"goapp/myapp/Utils/httpResp"
	"goapp/myapp/model"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

func AddSTAFF(w http.ResponseWriter, r *http.Request) {
	var staf model.Staff

	decoder := json.NewDecoder(r.Body)

	if err := decoder.Decode(&staf); err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, "invalid json body")
		return
	}

	defer r.Body.Close()
	saveErr := staf.Create()
	if saveErr != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, saveErr.Error())
		return
	}

	httpResp.ResponseWithJSON(w, http.StatusCreated, map[string]string{"status": "Staff Added"})
}

func getStaffId(staff_id string) (int64, error) {

	Sid, err := strconv.ParseInt(staff_id, 10, 64)

	if err != nil {
		return 0, err
	}
	return Sid, nil
}

func GetStaff(w http.ResponseWriter, r *http.Request) {
	sid := mux.Vars(r)["sid"]

	old_Id, Sid_err := getStaffId(sid)

	if Sid_err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, Sid_err.Error())
	}

	s := model.Staff{Staffid: old_Id}

	err_staff := s.GetSta()

	if err_staff != nil {

		switch err_staff {

		case sql.ErrNoRows:
			httpResp.RespondWithError(w, http.StatusNotFound, "Staff not found")

		default:
			httpResp.RespondWithError(w, http.StatusInternalServerError, err_staff.Error())
		}

		return
	}
	httpResp.ResponseWithJSON(w, http.StatusOK, s)

}

func GetAllSTaffs(w http.ResponseWriter, r *http.Request) {
	staffs, getErr := model.GetAllSta()
	if getErr != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, getErr.Error())
		return
	}
	// httpResp.RespondWithError(w, http.StatusBadRequest, getErr.Error())
	// return
	httpResp.ResponseWithJSON(w, http.StatusOK, staffs)
}

func DeleteStaff(w http.ResponseWriter, r *http.Request) {
	sid := mux.Vars(r)["sid"]
	staffid, idErr := getPlayerId(sid)
	if idErr != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, idErr.Error())
		return
	}

	s := model.Staff{Staffid: staffid}
	if err := s.Delete(); err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, err.Error())
		return
	}
	httpResp.ResponseWithJSON(w, http.StatusOK, map[string]string{"status": "Deleted"})
}

func GetTotalStaffs(w http.ResponseWriter, r *http.Request) {
	_, getErr := model.Gettotalst()
	if getErr != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, getErr.Error())
		return
	}

	totalStaffs, countErr := model.Gettotalst()
	if countErr != nil {
		httpResp.RespondWithError(w, http.StatusInternalServerError, countErr.Error())
		return
	}

	response := struct {
		Total int `json:"total"`
	}{
		Total: totalStaffs,
	}

	httpResp.ResponseWithJSON(w, http.StatusOK, response)
}
