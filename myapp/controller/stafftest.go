package controller

import (
	"bytes"
	"io"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestAddStaff(t *testing.T) {
	url := "http://localhost:8080/staff"

	var jsonStr = []byte(`{"sid":"1111", "fname":"Kinga", "lname":"Tshering", "specialization":7}`)

	req, _ := http.NewRequest("POST", url, bytes.NewBuffer(jsonStr))

	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)

	if err != nil {
		panic(err)
	}

	defer resp.Body.Close()

	body, _ := io.ReadAll(resp.Body)

	assert.Equal(t, http.StatusCreated, resp.StatusCode)
	expResp := `{"status": "player added"}`

	assert.JSONEq(t, expResp, string(body))
}

func TestGetStaff(t *testing.T) {
	c := &http.Client{}
	r, _ := c.Get("http://localhost:8080/staff/1111")
	body, _ := io.ReadAll(r.Body)
	assert.Equal(t, http.StatusOK, r.StatusCode)
	expResp := `{"pid":"1006", "fname":"Kinga", "lname":"Tshering", "specialization":7}`
	assert.JSONEq(t, expResp, string(body))
}

func TestDeleteStaff(t *testing.T) {
	url := "http://localhost:8080/staff/1111"

	req, _ := http.NewRequest("DELETE", url, nil)

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()

	body, _ := io.ReadAll(resp.Body)
	assert.Equal(t, http.StatusOK, resp.StatusCode)
	expResp := `{"status": "deleted"}`
	assert.JSONEq(t, expResp, string(body))

}

func TestStaffNotFound(t *testing.T) {
	assert := assert.New(t)
	c := &http.Client{}
	r, _ := c.Get("http://localhost:8080/staff/1006")
	body, _ := io.ReadAll(r.Body)
	assert.Equal(http.StatusNotFound, r.StatusCode)
	expResp := `{"error":"Student not found"}`
	assert.JSONEq(expResp, string(body))
}
