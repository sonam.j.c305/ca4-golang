package controller

import (
	"database/sql"
	"encoding/json"
	"goapp/myapp/Utils/httpResp"
	"goapp/myapp/model"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

func AddPlayer(w http.ResponseWriter, r *http.Request) {
	var stud model.Player

	decoder := json.NewDecoder(r.Body)

	if err := decoder.Decode(&stud); err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, "invalid json body")
		return
	}

	defer r.Body.Close()
	saveErr := stud.Create()
	if saveErr != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, saveErr.Error())
		return
	}

	httpResp.ResponseWithJSON(w, http.StatusCreated, map[string]string{"status": "Player Added"})
}

func getPlayerId(play_id string) (int64, error) {

	Pid, err := strconv.ParseInt(play_id, 10, 64)

	if err != nil {
		return 0, err
	}
	return Pid, nil
}

func GetPlayer(w http.ResponseWriter, r *http.Request) {
	pid := mux.Vars(r)["pid"]

	old_Id, Pid_err := getPlayerId(pid)

	if Pid_err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, Pid_err.Error())
	}

	s := model.Player{Playerid: old_Id}

	err_play := s.Getplee()

	if err_play != nil {

		switch err_play {

		case sql.ErrNoRows:
			httpResp.RespondWithError(w, http.StatusNotFound, "Player not found")

		default:
			httpResp.RespondWithError(w, http.StatusInternalServerError, err_play.Error())
		}

		return
	}
	httpResp.ResponseWithJSON(w, http.StatusOK, s)

}

func DeletePlayer(w http.ResponseWriter, r *http.Request) {
	pid := mux.Vars(r)["pid"]
	playid, idErr := getPlayerId(pid)
	if idErr != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, idErr.Error())
		return
	}

	s := model.Player{Playerid: playid}
	if err := s.Delete(); err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, err.Error())
		return
	}
	httpResp.ResponseWithJSON(w, http.StatusOK, map[string]string{"status": "Deleted"})
}

func GetAllPLayers(w http.ResponseWriter, r *http.Request) {
	players, getErr := model.GetAllPlay()
	if getErr != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, getErr.Error())
		return
	}
	// httpResp.RespondWithError(w, http.StatusBadRequest, getErr.Error())
	// return
	httpResp.ResponseWithJSON(w, http.StatusOK, players)
}

func GetTotalPlayers(w http.ResponseWriter, r *http.Request) {
	_, getErr := model.GetAllPlay()
	if getErr != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, getErr.Error())
		return
	}

	totalPlayers, countErr := model.Gettotal()
	if countErr != nil {
		httpResp.RespondWithError(w, http.StatusInternalServerError, countErr.Error())
		return
	}

	response := struct {
		Total int `json:"total"`
	}{
		Total: totalPlayers,
	}

	httpResp.ResponseWithJSON(w, http.StatusOK, response)
}
