package controller

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"goapp/myapp/Utils/httpResp"
	"goapp/myapp/model"
	"net/http"
	"time"

	"github.com/gorilla/mux"
)

func getAid(AdminIdParam string) (string, error) {
	return AdminIdParam, nil
}

func UpdateProfile(w http.ResponseWriter, r *http.Request) {
	old_cid := mux.Vars(r)["cid"]
	old_coId, idErr := getAid(old_cid)
	if idErr != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, idErr.Error())
		return
	}
	var cour model.Admin
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&cour); err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, "invalid json body")
		return
	}
	defer r.Body.Close()
	updateErr := cour.Update(old_coId)
	if updateErr != nil {
		switch updateErr {
		case sql.ErrNoRows:
			httpResp.RespondWithError(w, http.StatusNotFound, "User not found")
		default:
			httpResp.RespondWithError(w, http.StatusInternalServerError, updateErr.Error())
		}
	} else {
		httpResp.ResponseWithJSON(w, http.StatusOK, cour)
	}
}

func GetAdmin(w http.ResponseWriter, r *http.Request) {
	cookie, err := r.Cookie("my-cookie")

	if err != nil {
		if err == http.ErrNoCookie {
			httpResp.RespondWithError(w, http.StatusUnauthorized, err.Error())
		}
		fmt.Fprintln(w, "error reading cookie", err)
	}

	cid := cookie.Value
	fmt.Println(cid)

	c := model.Admin{CID: cid}

	getErr := c.Read()
	if getErr != nil {
		switch getErr {
		case sql.ErrNoRows:
			httpResp.RespondWithError(w, http.StatusNotFound, "Admin not found")
		default:
			httpResp.RespondWithError(w, http.StatusInternalServerError, getErr.Error())
		}
		return
	}
	httpResp.ResponseWithJSON(w, http.StatusOK, c)
}

func GetTotalAdmin(w http.ResponseWriter, r *http.Request) {
	_, getErr := model.GettotalAD()
	if getErr != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, getErr.Error())
		return
	}

	totalAdmins, countErr := model.GettotalAD()
	if countErr != nil {
		httpResp.RespondWithError(w, http.StatusInternalServerError, countErr.Error())
		return
	}

	response := struct {
		Total int `json:"total"`
	}{
		Total: totalAdmins,
	}

	httpResp.ResponseWithJSON(w, http.StatusOK, response)
}

func SignUp(w http.ResponseWriter, r *http.Request) {
	var admin model.Admin

	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&admin); err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, "invalid json body")
		return
	}
	defer r.Body.Close()

	saveErr := admin.Create()
	fmt.Printf("%+v", saveErr)

	if saveErr != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, saveErr.Error())
		return
	}
	// no error
	httpResp.ResponseWithJSON(w, http.StatusCreated, map[string]string{"status": "admin added"})
}

func Login(w http.ResponseWriter, r *http.Request) {
	var admin model.Admin
	err := json.NewDecoder(r.Body).Decode(&admin)
	if err != nil {
		httpResp.RespondWithError(w, http.StatusBadRequest, "invalid json body")
		return
	}

	defer r.Body.Close()

	getErr := admin.Get()
	if getErr != nil {
		httpResp.RespondWithError(w, http.StatusUnauthorized, getErr.Error())
		return
	}

	cookie := http.Cookie{
		Name:    "my-cookie",
		Value:   admin.CID,
		Expires: time.Now().Add(30 * time.Minute),
		Secure:  true,
	}
	http.SetCookie(w, &cookie)
	// no error
	httpResp.ResponseWithJSON(w, http.StatusOK, map[string]string{"message": "login success"})
}

func Logout(w http.ResponseWriter, r *http.Request) {
	http.SetCookie(w, &http.Cookie{
		Name:    "my-cookie",
		Expires: time.Now(),
	})

	httpResp.ResponseWithJSON(w, http.StatusOK, map[string]string{"message": "cookie deleted"})
}

func VerifyCookie(w http.ResponseWriter, r *http.Request) bool {
	cookie, err := r.Cookie("my-cookie")
	if err != nil {
		if err == http.ErrNoCookie {
			httpResp.RespondWithError(w, http.StatusSeeOther, "cookie no found")
			return false
		}
		httpResp.RespondWithError(w, http.StatusInternalServerError, "internal server error")
		return false
	}
	if cookie.Value != "my-value" {
		httpResp.RespondWithError(w, http.StatusSeeOther, "cookie does not match")
		return false
	}
	return true
}
